﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galgje;

public partial class SimpleButton : Label
{
    public delegate void ClickEventHandler(object sender, RoutedEventArgs e);

    public event ClickEventHandler? Click;

    public Brush AccentColor { get; set; } = Brushes.Crimson;

    public SimpleButton()
    {
        InitializeComponent();
    }

    private void ChangeBorderOnMouseEnter(object sender, MouseEventArgs e)
    {
        this.BorderBrush = AccentColor;
    }

    private void ChangeBorderOnMouseLeave(object sender, MouseEventArgs e)
    {
        if (!IsKeyboardFocused)
        {
            this.BorderBrush = Brushes.Gray;
        }
    }

    private void ThisMouseDown(object sender, MouseButtonEventArgs e)
    {
        if (Click is not null)
        {
            Click(this, e);
            e.Handled = true;
        }
    }

    private void Label_KeyDown(object sender, KeyEventArgs e)
    {
        if (Click is not null && (e.Key == Key.Enter || e.Key == Key.Space))
        {
            Click(this, e);
            e.Handled = true;
        }
    }

    private void SimpleButton_OnGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
    {
        this.BorderBrush = AccentColor;
    }

    private void SimpleButton_OnLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
    {
        if (!IsMouseOver)
        {
            this.BorderBrush = Brushes.Gray;
        }
    }
}