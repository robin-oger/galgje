﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galgje;

/// <summary>
/// Interaction logic for HighScores.xaml
/// </summary>
public partial class HighScores : UserControl
{
    readonly List<Score> scoresRef;

    public HighScores(List<Score> scores)
    {
        InitializeComponent();

        Render(scores);

        scoresRef = scores;
    }

    /// <summary>
    /// Shows the list of scores on screen
    /// </summary>
    /// <param name="scores"></param>
    private void Render(List<Score> scores)
    {
        ScoresListBox.Items.Clear();

        var orderedByLength = scores.OrderBy(score => score.GameLength);
        
        foreach (var score in orderedByLength)
        {
            ScoresListBox.Items.Add(RenderScore(score));
        }
    }

    /// <summary>
    /// Turns a score object into a ListBoxItem
    /// </summary>
    /// <param name="score"></param>
    /// <returns>The ListBoxItem</returns>
    private ListBoxItem RenderScore(Score score)
    {
        var stack = new StackPanel()
        {
            Orientation = Orientation.Horizontal,
            VerticalAlignment = VerticalAlignment.Center,
            HorizontalAlignment = HorizontalAlignment.Center,
        };
        stack.Children.Add(new TextBlock()
        {
            VerticalAlignment = VerticalAlignment.Center,
            TextAlignment = TextAlignment.Center,
            HorizontalAlignment = HorizontalAlignment.Center,
            FontSize = 22,
            Margin = new Thickness(0, 0, 20, 0),
            Text = $"{score.PlayerName} - {score.LivesLost} levens ({score.GameLength.TotalSeconds:0.##}s)"
        });

        var button =
            new SimpleButton()
            {
                Content = "🗑",
            };

        var scoreToRemove = score;
        button.Click += delegate { Remove(scoreToRemove); };

        stack.Children.Add(
            button
        );


        return
            new()
            {
                Content = stack,
                HorizontalAlignment = HorizontalAlignment.Center,
            };
    }

    /// <summary>
    /// Removes a score object from the list of scores, then redraws the list
    /// </summary>
    /// <param name="score"></param>
    /// <exception cref="Exception"></exception>
    private void Remove(Score score)
    {
        if (!scoresRef.Remove(score))
        {
            throw new Exception("tried to remove a score that doesn’t exist, shouldn’t happen");
        }

        Render(scoresRef);
    }

    private void RemoveIndices(IEnumerable<int> indices)
    {
                                                                // toArray() very important, 
                                                                // otherwise it’s not going to iterate over it all 
                                                                // before removing certain indices therefore invaliding
                                                                // the list
        foreach (var score in indices.Select(i => scoresRef[i]).ToArray())
        {
            if (!scoresRef.Remove(score))
            {
                throw new Exception("tried to remove a score that doesn’t exist, shouldn’t happen");
            }
        }
        Render(scoresRef);
    }

    /// <summary>
    /// Deletes the selected items when Delete key is pressed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ScoresListBox_OnKeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key is Key.Delete)
        {
            List<int> indices = new();
            foreach (var obj in ScoresListBox.SelectedItems)
            {
                var idx = ScoresListBox.Items.IndexOf(obj);
                indices.Add(idx);
            }
            
            RemoveIndices(indices);
        }
    }
}