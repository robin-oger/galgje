using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace Galgje;

/// <summary>
/// Dialog allowing the user to choose one integer in an arbitrary range
/// </summary>
public partial class IntegerDialog : Window
{
    private readonly int min;
    private readonly int max;

    private int currentValue;
    public int CurrentValue
    {
        get => currentValue;
        set
        {
            currentValue = value;
            NumberOutput.Text = value.ToString();
            IncrementButton.IsEnabled = (value != max);
            DecrementButton.IsEnabled = (value != min);
        }
        
    }

    private IntegerDialog(int min, int max, int current)
    {
        InitializeComponent();
        
        this.min = min;
        this.max = max;

        this.DataContext = this;

        if (current < min || current > max)
        {
            throw new ArgumentOutOfRangeException($"{nameof(current)} is not contained in ");
        }

        if (min >= max)
            throw new ArgumentOutOfRangeException($"{nameof(min)} can’t be bigger or equal to {nameof(max)}");


        CurrentValue = current;
    }

    public static int? AskNumber(string prompt, int current, int min = Int32.MinValue, int max = Int32.MaxValue, Window? owner = null)
    {
        var window = new IntegerDialog(min, max, current)
        {
            Title = prompt,
            Owner = owner,
            WindowStartupLocation = WindowStartupLocation.CenterOwner,
        };

        var res = window.ShowDialog();


        return res switch
        {
            true => window.currentValue,
            _ => null,
        };
    }

    private void DecrementButton_OnClick(object sender, RoutedEventArgs e)
    {
        --CurrentValue;
    }

    private void IncrementButton_OnClick(object sender, RoutedEventArgs e)
    {
        ++CurrentValue;
    }

    private void CancelButton_OnClick(object sender, RoutedEventArgs e)
    {
        DialogResult = false;
        Close();
    }

    private void ValidateButton_OnClick(object sender, RoutedEventArgs e)
    {
        DialogResult = true;
        Close();
    }
}