﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galgje;
/// <summary>
/// Interaction logic for StartGame.xaml
/// </summary>

public partial class StartGame : UserControl
{
    public delegate void GameStartedHandler(string secretWord);
    public event GameStartedHandler? GameStarted;

    public StartGame()
    {
        InitializeComponent();
        SecretWordBox.Focus();
    }

    private void Start()
    {
        var word = SecretWordBox.Text;
        GameStarted?.Invoke(word);
    }


    private void StartGameButton_Click(object sender, RoutedEventArgs e)
        => Start();

    private void SecretWordBox_TextChanged(object sender, TextChangedEventArgs e)
    {
        var filtered = string.Join("", SecretWordBox.Text.Where(char.IsLetter));
        var diff = SecretWordBox.Text.Length - filtered.Length;
        // try to put back the carret at the correct place
        if (diff != 0)
        {
            var old = SecretWordBox.CaretIndex;
            SecretWordBox.Text = filtered;
            SecretWordBox.CaretIndex = old - diff;
        }

        if (SecretWordBox.Text.Length > 0)
        {
            StartGameButton.IsEnabled = true;
        }
        else
        {
            StartGameButton.IsEnabled = false;
        }

    }

    private void SecretWordBox_KeyDown(object sender, KeyEventArgs e)
    {
        if (StartGameButton.IsEnabled && e.Key == Key.Enter)
            Start();
    }
}
