﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

using System.Text.Json;
using System.Diagnostics;

namespace Galgje;
/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private const string ScoresDBPath = "scoresDB.json";
    private List<Score> scores;

    private int chosenGuessInterval = 10;

    public static readonly RoutedCommand HintCmd = new RoutedCommand();
    public static readonly RoutedCommand TimerCmd = new RoutedCommand();
    public static readonly RoutedCommand HighscoresCmd = new RoutedCommand();

    private new UserControl Content
    {
        set => CurrentPage.Content = value;
    }

    public MainWindow()
    {
        InitializeComponent();

        scores = LoadScores();
        SetLauncherPage();
    }


    /// <summary>
    /// Tries to load the scores from a file,
    /// otherwise returns an empty list.
    /// </summary>
    /// <returns></returns>
    private static List<Score> LoadScores()
    {
        try
        {
            var json = File.ReadAllText(ScoresDBPath);
            var value = JsonSerializer.Deserialize<List<Score>>(json);

            if (value is { } scores)
                return scores;
            else
                throw new Exception("Deserializer returned null");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"failed to load the scores db: {ex}");
            return new();
        }
    }

    /// <summary>
    /// Saves the score list in a json file
    /// </summary>
    /// <param name="scores"></param>
    private static void SaveScores(List<Score> scores)
    {
        var json = JsonSerializer.Serialize(scores);
        File.WriteAllText(ScoresDBPath, json);
    }

    private void SetNewGamePage(Launcher.GameType gameType)
    {
        if (gameType is Launcher.GameType.TwoPlayers)
        {
            var newGamePage = new StartGame();
            Content = newGamePage;

            newGamePage.GameStarted += SetGamePage;
        }
        else if (gameType is Launcher.GameType.SinglePlayer)
        {
            var words = File.ReadAllLines("Wordlist.txt");

            var secretWord = words[new Random().Next(words.Length)];
            SetGamePage(secretWord);
        }
    }

    private void SetLauncherPage()
    {
        var launcher = new Launcher();
        Content = launcher;

        launcher.NewGameStarted += SetNewGamePage;
    }

    private void SetGamePage(string word)
    {
        var gamePage = new Game(word, chosenGuessInterval);
        Content = gamePage;

        gamePage.NewGame += SetLauncherPage;
        gamePage.GameEnded += SetEndGamePage;
    }

    private void SetEndGamePage(Game.EndKind endKind, string word, DateTime startTime, int livesLost)
    {
        var endPage = new EndGameControlPage(endKind, word, startTime, livesLost);
        Content = endPage;

        endPage.NewGame += SetLauncherPage;
        endPage.ScoreSaved += SaveHighScore;
        endPage.RequestClose += Close;
    }

    private void SaveHighScore(Score score)
    {
        scores.Add(score);
    }

    private void SetHighScorePage()
    {
        Content = new HighScores(scores);
    }

    private void Window_Closed(object sender, EventArgs e)
    {
        SaveScores(scores);
    }

    private void NewGameMenuEntry_Click(object sender, RoutedEventArgs e)
    {
        SetLauncherPage();
    }

    private void ExitMenuEntry_Click(object sender, RoutedEventArgs e)
    {
        Close();
    }

    private void TimerCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = CurrentPage?.Content is Launcher;
    }

    private void TimerCommand_Executed(object sender, ExecutedRoutedEventArgs e)
    {
        var res = IntegerDialog.AskNumber("Timer instellingen", chosenGuessInterval, 5, 20, this);

        if (res is { } number)
            chosenGuessInterval = number;
    }

    private void HighscoresCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = true;
    }

    private void HighscoresCommand_Executed(object sender, ExecutedRoutedEventArgs e)
    {
        SetHighScorePage();
    }
}
