﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galgje;

/// <summary>
/// Score class storing information about a past game
/// </summary>
public class Score
{
    public TimeSpan GameLength{ get; set; }
    public int LivesLost { get; set; }
    public string PlayerName{ get; set; }


    public Score(TimeSpan gameLength, int livesLost, string playerName)
    {
        GameLength = gameLength;
        LivesLost = livesLost;
        PlayerName = playerName;   
    }

    public override string ToString()
    {
        return $"{GameLength}, {LivesLost}, {PlayerName}";
    }
}
