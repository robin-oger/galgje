﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Diagnostics;
using System.Windows.Media.Animation;

namespace Galgje;
/// <summary>
/// Interaction logic for Game.xaml
/// </summary>
public partial class Game : UserControl
{
    public delegate void GameEndedHandler(EndKind endKind, string wordToGuess, DateTime startTime, int livesLost);
    public event GameEndedHandler? GameEnded;

    public event Action? NewGame;

    public const int MaxLives = 10;

    private int guessInterval = 10;
    private char? currentChar;

    private readonly List<char> guesses = new();
    private readonly List<char> hints = new();

    private bool matchingWholeWord = false;
    private int wholeMatchErrors = 0;
    private int previousErrorCount = 0;

    private readonly string secretWord;

    private readonly DateTime startTime = DateTime.Now;
    private DateTime lastGuess = DateTime.Now;

    private Image[] layers;

    private DispatcherTimer timer;

    private int livesLost = 0;
    private bool hasCoolDown = false;

    public Game(string word, int guessInterval = 10)
    {
        InitializeComponent();

        this.guessInterval = guessInterval;
        layers = LoadLayers();

        currentChar = null;
        secretWord = word.ToUpper();
        ComputeOutput();

        timer = new DispatcherTimer
        {
            IsEnabled = true,
            Interval = TimeSpan.FromMilliseconds(10),
        };
        timer.Tick += delegate { CalculateTimeSpent(); };

        CalculateTimeSpent();
    }

    /// <summary>
    /// Takes care of loading the different hungman layers 
    /// from the assets directory
    /// </summary>
    /// <returns>The assets in an array of Image objects</returns>
    private static Image[] LoadLayers()
    {
        Debug.Assert(Directory.Exists("assets"));

        var layers = new Image[10];

        int i = 0;
        foreach (var file in Directory.EnumerateFiles("assets"))
        {
            var uri = new Uri(file, UriKind.Relative);
            layers[i++] = new Image { Source = new BitmapImage(uri) };
        }

        Debug.Assert(i == 10);

        return layers;

    }

    private void CalculateTimeSpent()
    {
        var sinceLastGuess = DateTime.Now - lastGuess;
        if (hasCoolDown && sinceLastGuess <= TimeSpan.FromSeconds(1))
            return;
        var timeRemaining = TimeSpan.FromSeconds(guessInterval + (hasCoolDown ? 1 : 0)) - sinceLastGuess;

        if (timeRemaining < TimeSpan.FromSeconds(0))
        {
            hasCoolDown = true;
            wholeMatchErrors += 1;
            ComputeOutput();
            lastGuess = DateTime.Now;
        }


        var seconds = Math.Max(0, Math.Round(timeRemaining.TotalSeconds % 60));
        var output = $"{timeRemaining.Minutes.ToString().PadLeft(2, '0')}:{seconds.ToString().PadLeft(2, '0')}";

        TimeSpentTextBlock.Text = output;
    }

    /// <summary>
    /// Guesses user input against secret word
    /// </summary>
    /// <exception cref="NotImplementedException"></exception>
    private void Guess()
    {
        if (matchingWholeWord)
        {
            GuessWholeWord(GuessingTextBox.Text);
        }
        else if (currentChar is char ch)
        {
            guesses.Add(ch);
        }
        else
        {
            //If this happens, it means we are guessing a single character with empty input,
            //which isn’t supposed to happen
            throw new NotImplementedException("unreachable");
        }

        lastGuess = DateTime.Now;


        ComputeOutput();
        GuessingTextBox.Text = string.Empty;

    }

    private void GuessWholeWord(string word)
    {
        if (secretWord.ToLower() == word.ToLower())
        {
            EndGame(won: true);
        }
        else
        {
            wholeMatchErrors += 1;
        }
    }


    /// <summary>
    /// Takes care of displaying the animation whenever the user lost a life,
    /// however that may be
    /// </summary>
    private void LostLifeAnimation()
    {
        var color = Colors.Transparent;
        var errorColor = Colors.Red;
        this.Background = new SolidColorBrush(color);

        var animation = new ColorAnimation
        {
            From = color,
            To = errorColor,
            Duration = TimeSpan.FromMilliseconds(300),
            AutoReverse = true,
        };

        animation.Completed += delegate { Debug.WriteLine("Life depleted animation completed"); };

        this.Background.BeginAnimation(SolidColorBrush.ColorProperty, animation);

    }

    /// <summary>
    /// This happens anytime the state changes, and takes care 
    /// of displaying it to the user.
    /// </summary>
    private void ComputeOutput()
    {
        var errors = new List<char>();

        foreach (var guess in guesses)
        {
            if (!secretWord.Contains(guess))
                errors.Add(guess);
        }

        var errorCount = errors.Count + wholeMatchErrors;
        livesLost = errorCount;
        var lives = MaxLives - errorCount;

        ImageContainer.Children.Clear();
        foreach (var layer in layers.Take(errorCount + 1))
        {
            ImageContainer.Children.Add(layer);
        }

        if (previousErrorCount < errorCount)
        {
            LostLifeAnimation();
        }

        previousErrorCount = errorCount;

        const string heart = "❤️";
        var hearts = string.Join(" ", Enumerable.Repeat(heart, lives));

        LivesTextBox.Text = hearts;
        ErrorsTextBox.Text = $"{string.Join(" - ", errors)}";
        if(hints.Count > 0)
        {
            ErrorsTextBox.Text += $"\nHints: {string.Join(" - ", hints)}";
        }

        var output = "";
        var isGuessed = true;
        foreach (var letter in secretWord)
        {
            if (guesses.Contains(letter))
            {
                output += letter;
            }
            else
            {
                output += "▯";
                isGuessed = false;
            }
        }

        SecretWordTextBox.Text = output;

        if (isGuessed)
        {
            EndGame(won: true);
        }
        else if (lives <= 0)
        {
            EndGame(won: false);
        }

    }

    /// <summary>
    /// Ends the game by stopping the timer and sending an event to the parent
    /// </summary>
    /// <param name="won"></param>
    private void EndGame(bool won)
    {
        Clean();

        var endKind = (won, hints.Count == 0) switch
        {
            (true, true) => EndKind.Win,
            (true, false) => EndKind.InvalidWin,
            (false, _) => EndKind.Loss,
        };

        Console.WriteLine($"endkind: {endKind}");

        GameEnded?.Invoke(endKind, secretWord, startTime, livesLost);
    }

    /// <summary>
    /// Call this clean resources before abandonning the object
    /// </summary>
    private void Clean()
    {
        timer.Stop();
    }


    private void GuessButton_Click(object sender, RoutedEventArgs e)
    {
        Guess();
    }

    /// <summary>
    /// If the user didn’t choose to match a whole word, this 
    /// insures that there’s never more than one letter in the 
    /// text box, otherwise it will keep it as is
    /// 
    /// In any case, this also insures that 
    /// the input is composed of letters and nothing else
    /// </summary>
    private void SanitizeTextBox()
    {
        var text = GuessingTextBox.Text;
        var carret = GuessingTextBox.CaretIndex;
        if (matchingWholeWord)
        {
            var filtered = string.Join("", text.Where(char.IsLetter));
            var diff = text.Length - filtered.Length;

            GuessButton.IsEnabled = filtered.Length > 0;
            GuessingTextBox.Text = filtered.ToUpper();
            GuessingTextBox.CaretIndex = carret - diff;
            return;
        }
        else
        {
            if (text.Length == 0)
            {
                currentChar = null;
            }
            else
            {
                if (carret == 0)
                    carret = 1;

                var ch = text[carret - 1];

                if (char.IsLetter(ch))
                {
                    text = text[(carret - 1)..carret].ToUpper();
                    currentChar = ch;
                }
                else
                {
                    text = string.Empty;
                    currentChar = null;
                }

                GuessingTextBox.Text = text;
                GuessingTextBox.CaretIndex = 1;
            }
        }

        GuessButton.IsEnabled = currentChar != null;
    }

    private void GuessingTextBox_TextChanged(object sender, TextChangedEventArgs e)
    {
        SanitizeTextBox();
    }


    private void GuessingTextBox_KeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.Enter && GuessButton.IsEnabled)
        {
            Guess();
            e.Handled = true;
        }
    }

    private void NewGameButton_Click(object sender, RoutedEventArgs e)
    {
        Clean();
        NewGame?.Invoke();
    }

    private void MatchWholeWord_Click(object sender, RoutedEventArgs e)
    {
        matchingWholeWord = MatchWholeWord.IsChecked ?? false;
        SanitizeTextBox();
    }

    private char[] GetHintPossibilities()
    {
        const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var possibilities = alphabet.Except(secretWord).Except(guesses).Except(hints).ToArray();
        return possibilities;
    }

    /// <summary>
    /// Adds a letter that isn’t in the world or in the guess list to the list of hints
    /// </summary>
    private void  GenerateHint()
    {
        var possibilities = GetHintPossibilities();

        //TODO: make this hide the CanExecute flag of the Hint command
        if(possibilities.Length == 0)
        {
            throw new Exception("can’t generate a hint");
        }

        Console.WriteLine($"possibilities: {string.Join(", ", possibilities)}");

        var rng = new Random();

        char ch = possibilities[rng.Next(possibilities.Length)];
        Console.WriteLine(ch);

        hints.Add(ch);
        ComputeOutput();
    }

    private void HintCommand_Executed(object sender, ExecutedRoutedEventArgs e)
    {
        GenerateHint();
    }

    private void HintCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = GetHintPossibilities().Length != 0;
    }

    private void UserControl_Loaded(object sender, RoutedEventArgs e)
    {
        GuessingTextBox.Focus();
    }

    public enum EndKind
    {
        Win,
        InvalidWin,
        Loss,
    }

    private void UserControl_Unloaded(object sender, RoutedEventArgs e)
    {
        //insures the timer doesn’t run when the  game was ended by the user
        Clean();
    }
}
