﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galgje;
/// <summary>
/// Interaction logic for EndGameControlPage.xaml
/// </summary>
public partial class EndGameControlPage : UserControl
{
    public event Action? RequestClose;
    public event Action? NewGame;
    public event Action<Score>? ScoreSaved;

    private TimeSpan timeSpent;
    private int livesLost;

    public EndGameControlPage(Game.EndKind endKind, string wordToGuess, DateTime gameStartTime, int livesLost)
    {
        InitializeComponent();

        var timeItTook = DateTime.Now - gameStartTime;

        timeSpent = timeItTook;
        this.livesLost = livesLost;

        Console.WriteLine($"now {endKind}");

        switch(endKind)
        {
            case Game.EndKind.Win:
                OutputTextBlock.Text = $"Gefeliciteerd! U heeft correct het woord « {wordToGuess} » gegokt!";
                break;
            case Game.EndKind.InvalidWin:
                OutputTextBlock.Text = $"U heeft correct het woord « {wordToGuess} » gegokt, maar u heeft hints gebruikt u kunt daarom uw score niet opgeslaan worden";
                ScoreSavingStackPanel.Visibility = Visibility.Collapsed;
                break;
            case Game.EndKind.Loss:
                OutputTextBlock.Text = $"Jammer, het woord was «{wordToGuess}»";
                ScoreSavingStackPanel.Visibility = Visibility.Collapsed;
                break;
        }

        OutputTextBlock.Text += $"\nHet spelletje duurde {Math.Round(timeItTook.TotalSeconds, 2)} seconden.";
    }

    private void CloseButton_Click(object sender, RoutedEventArgs e)
    {
        RequestClose?.Invoke();
    }

    private void StartANewGameButton_Click(object sender, RoutedEventArgs e)
    {
        NewGame?.Invoke();
    }

    private void SaveScoreButton_Click(object sender, RoutedEventArgs e)
    {

        var control = (Control)sender;

        if(PlayerNameDialog.AskPlayerName("Wat is uw naam?", this.Parent as Window) is {} result)
        {
            ScoreSaved?.Invoke(new(timeSpent, livesLost, result));
            control.IsEnabled = false;
        }

    }
}
