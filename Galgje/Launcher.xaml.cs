﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galgje;
/// <summary>
/// Interaction logic for Launcher.xaml
/// </summary>
public partial class Launcher : UserControl
{
    public enum GameType
    {
        SinglePlayer,
        TwoPlayers,
    }

    public Launcher()
    {
        InitializeComponent();
    }

    public event Action<GameType>? NewGameStarted;

    private void StartANewGameButton_Click(object _, RoutedEventArgs e)
        => NewGameStarted?.Invoke(GameType.TwoPlayers);

    private void SinglePlayerButton_Click(object sender, RoutedEventArgs e)
        => NewGameStarted?.Invoke(GameType.SinglePlayer);
}
