﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Galgje
{
    /// <summary>
    /// Interaction logic for PlayerNameDialog.xaml
    /// </summary>
    public partial class PlayerNameDialog : Window
    {
        string? Result = null;

        private PlayerNameDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Creates a dialog asking for the player name, 
        /// if the operation is valided, returns a string,
        /// otherwise, returns null.
        /// </summary>
        /// <param name="prompt">
        /// The title of the created dialog
        /// </param>
        /// <returns></returns>
        public static string? AskPlayerName(string prompt, Window? owner = null)
        {
            var window = new PlayerNameDialog
            {
                Title = prompt,
                Owner = owner,
                WindowStartupLocation = WindowStartupLocation.CenterOwner,
            };
            window.ShowDialog();
            return window.Result;
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Enter)
                Validate();
        }

        private void Validate()
        {
            Result = PlayerName.Text;
            Close();
        }

        private void ValidateButton_Click(object sender, RoutedEventArgs e)
        {
            Validate();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Result = null;
            Close();
        }
    }
}
